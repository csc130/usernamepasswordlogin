import javax.swing.JOptionPane;

public class Authentication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String correctUser = "Test";
		String correctPw = "123";
		String correctType = "Student";
		String username;
		String pw;
		int count = 0;
		final int LIMIT = 3;

		do {
			// Pass Test Welcome when correct uname and pw
			username = JOptionPane.showInputDialog("Enter user name, Trial:"
					+ (count + 1));
			if (!username.equalsIgnoreCase(correctUser))
				count++;
		} while (!username.equalsIgnoreCase(correctUser) && count < LIMIT);

		if (count < LIMIT) {
			do {
				// Pass Test Welcome when correct uname and pw
				pw = JOptionPane.showInputDialog("Enter password, Trial:"
						+ (count + 1));
				if (!pw.equals(correctPw))
					count++;
			} while (!pw.equals(correctPw) && count < LIMIT);
			if (count < LIMIT) {
				// Pass Test Welcome when correct uname and pw
				JOptionPane.showMessageDialog(null, "Welcome " + username);
				// Part2: Account Type
				String[] choices = { "Admin", "Student", "Staff" };
				String input = (String) JOptionPane
						.showInputDialog(null, "Choose account type...",
								"Account Type", JOptionPane.QUESTION_MESSAGE,
								null, choices, choices[1]);
				// loop until account type is correct
				while (!input.equalsIgnoreCase(correctType)) {
					input = (String) JOptionPane.showInputDialog(null,
							"Choose account type...", "Account Type",
							JOptionPane.QUESTION_MESSAGE, null, choices,
							choices[1]);
				}
				
				switch (input) {
				case "Admin":
					JOptionPane.showMessageDialog(null, "Welcome Admin");
					break;
				case "Student":
					JOptionPane.showMessageDialog(null, "Welcome Student");
					break;
				case "Staff":
					JOptionPane.showMessageDialog(null, "Welcome Staff");
					break;
				}

			} else {
				// Pass Test invalid username 1, invalid password 2
				JOptionPane.showMessageDialog(null, "Contact Admin! Trial:"
						+ (count + 1));
			}
		} else {
			// Pass Test when incorrect uname 3 trials
			JOptionPane.showMessageDialog(null, "Invalid Username: Trial:"
					+ (count + 1));
		}
	}
}
